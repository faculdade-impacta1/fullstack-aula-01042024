# from typing import List
# from app.db.deps import get_db_session
# from fastapi import APIRouter, Response, Depends, status, Query, HTTPException
# from sqlalchemy.orm import Session
# from db.database import engine,SessionLocal, get_db
# from db.models import Usuarios as UsuariosModel
# from schemas.usuario import Usuarios, UsuariosRequest, UsuariosResponse
# from sqlalchemy.orm import Session
# from repository.usuario import UsuarioRepository
# from fastapi.security import OAuth2PasswordRequestForm
# from db.base import Base
# from db.database import get_db


# #cria a tabela
# Base.metadata.create_all(bind=engine)
# router = APIRouter(prefix="/v1/api/usuarios")

from fastapi import HTTPException, status, Response, Depends, APIRouter
from db.database import engine
from db.deps import get_db_session
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
from sqlalchemy.orm import Session
from repository.usuario import UsuariosRepository
from db.models import Usuarios as UsuariosModel
from schemas.usuario import Usuarios, UsuariosRequest, UsuariosResponse
router = APIRouter(prefix='/user')

@router.post("/register", status_code=status.HTTP_201_CREATED)
def create(request: UsuariosRequest, db: Session = Depends(get_db_session)):
    ok= UsuariosRepository.save(db, UsuariosModel(**request.dict()))
    return "ok"
    
@router.get("/procurar_por_id/{username}", response_model=UsuariosResponse)
def find_by_name(username: str, db: Session = Depends(get_db_session)):
    usuario = UsuarioRepository.find_by_name(db, username)
    if not usuario:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Produto não encontrado"
        )
    return UsuariosResponse.from_orm(usuario)
    

@router.post('/login')
def user_login(
    login_request_form: OAuth2PasswordRequestForm = Depends(),
    db_session: Session = Depends(get_db_session)
):
    uc = UsuariosRepository(db_session=db_session)

    user = Usuarios(
        username=login_request_form.username,
        password=login_request_form.password
    )

    token_data = uc.user_login(user=user, expires_in=60)

    return token_data
# @router.get("/testar")
# def validador():
#     teste = UsuariosRepository.validador()




# @router.get("/listar_todos", response_model=list[UsuariosResponse])
# def find_all(db: Session = Depends(get_db)):
#     usuarios = UsuarioRepository.find_all(db)
#     return [UsuariosResponse.from_orm(usuario) for usuario in usuarios]


# @router.get("/procurar_por_id/{id}", response_model=UsuariosResponse)
# def find_by_id(id: int, db: Session = Depends(get_db)):
#     usuario = UsuarioRepository.find_by_id(db, id)
#     if not usuario:
#         raise HTTPException(
#             status_code=status.HTTP_404_NOT_FOUND, detail="usuario não encontrado"
#         )
#     return UsuariosResponse.from_orm(usuario)


# @router.delete("/delete/{id}", status_code=status.HTTP_204_NO_CONTENT)
# def delete_by_id(id: int, db: Session = Depends(get_db)):
#     if not UsuarioRepository.exists_by_id(db, id):
#         raise HTTPException(
#             status_code=status.HTTP_404_NOT_FOUND, detail="Usuario não encontrado"
#         )
#     UsuarioRepository.delete_by_id(db, id)
#     return Response(status_code=status.HTTP_204_NO_CONTENT)


# @router.put("/update/{id}", response_model=UsuariosResponse)
# def update(id: int, request: UsuariosRequest, db: Session = Depends(get_db)):
#     usuario = UsuarioRepository.save(db, UsuariosModel(id=id, **request.dict()))
#     return UsuariosResponse.from_orm(usuario)

